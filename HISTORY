Version 0.6 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------

* add Python 3 compatibility.
* minimum required Python version is now Python 2.7.
* Rework test-suite.


Version 0.5 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
:sieveshell:
  - Changed default port for the MANAGESIEVE protocol to 4190 as
    proposed by RFC 5804. Thanks to Guido Berhoerster for submitting
    the patch.
  - Added option ``--port``. Thanks to Damien Aumaitre for submitting
    the patch and to Guido Berhoerster for an enhancement.
  - Added option ``--verbose`` for controlling debug output in
    managesieve. May be given several times to increase verbosity.

:managesieve:
  - Switched to Python standard logging system.
    This introduces a minor interface change: MANAGESIEVE.debug and
    global Debug are gone. See source for information about debugging
    log levels.


Version 0.4.2 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
:managesieve:
  - Use ssl.wrap_socket() instead of deprecated socket.ssl().
    Thanks to Guido Berhoerster for submitting the patch.

Version 0.4.1 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
:managesieve:
  - fixed short read (thanks to paurkedal for submitting the patch)


Version 0.4 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
:managesieve:
  - now work with Python 2.3 and later
  - added support for TLS (STARTTLS), special thanks to Gregory Boyce
    for fixing some corner cases here
  - added support for PLAIN authentication
  - use optparse if available instead of optik.
  - API change: login() no longer uses the LOGIN authentication
    mechanism, but has become a  convenience function. It uses the best
    mechanism available for authenticating the user.

  Bugfixes:
  - If a capability had no values (like 'STARTTLS'), parsing
    capabilities failed. Fixed.
  - removed dependency on some imaplib's private functions
  - fixed typo: self.supports_tls

  Thanks to Tomas 'Skitta' Lindroos, Lorenzo Boccaccia, Alain Spineux,
  darkness and Gregory Boyce for sending patches.

:sieveshell:
  - added support for different authentication mechanisms
  - added option --start-tls
  - more verbose output when server says "BYE" (prints out referrals
    etc.)
  - automatic cmd_quit() when server says "BYE"
  - several minor bugfixes and enhancements


Version 0.3 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
:managesieve:
  - MANAGESIEVE.authenticate() now only returns a OK/NO/BYE result
    like any command not asking data from the server

:sieveshell:
  - added 'edit', which may create scripts, too. (posix only)
  - now prints out the server capabilities, thus the user knows what
    the server is capable of (and which Sieve-Commands may be used).
  - fixed some minor bugs


Version 0.2 by Hartmut Goebel <h.goebel at crazy-compilers.com>
--------------------------------------------------------------------
- renamed to 'managesieve'
- added sieveshell
- major overhaul
- added support for HAVESPACE, CAPABILITY
- added unittest testsuite (covers most cases)


Version 0.1 by Ulrich Eck <ueck at net-labs.de>
--------------------------------------------------------------------
- initial release of Sieve.py by Ulrich Eck as part of 'ImapClient'
  (see http://www.zope.org/Members/jack-e/ImapClient), a Zope product.

..
  Local Variables:
  mode: rst
  ispell-local-dictionary: "american"
  End:
